import {argv} from 'yargs'
import * as docx from 'html-docx-js'
import * as fs from 'fs-extra'
import * as moment from 'moment'
import * as path from 'path'
import * as pug from 'pug'
import * as puppeteer from 'puppeteer'
import * as request from 'request-promise-native'
import * as sass from 'node-sass'
import * as xlsx from 'xlsx'

if (argv.h || argv.help) {
	console.log(
		`Asana Invoice Generator - A tool for creating invoices from comments in Asana tasks.\n` +
		`  Usage: node source/serverSideScript.js [...flags]\n` +
		`\n` +
		`  Flags:\n` +
		`    -t, --taskId=          (optional) The id of the Asana task to take the comments from, defaults to the one provided in the config file.\n` +
		`    -d, --date=            (optional) A date in the format YYYY-MM-DD to be used to calculate the the start and end range of the comments. Defaults to the current date. I.e. 2020-03-10 will get the comments from the start to the end of March 2020\n` +
		`    --xlsx                 (optional) Whether the program should generate an XLSX file with the invoice services data.\n` +
		`    --docx                 (optional) Whether the program should generate a complete DOCX invoice file with all invoice data.\n` +
		`    --pdf                  (optional) Whether the program should generate a set of PDF files (invoice and invoice with services).\n` +
		`\n` +
		`    -h, --help             Displays this information.\n` +
		`\n`
	)
	process.exit(0)
}

;(async function() {
	let config: {
		authToken: string
		taskId?: string
		templateData: {
			companyAddressLine0Bulgarian: string
			companyAddressLine0English: string
			companyAddressLine1Bulgarian: string
			companyAddressLine1English: string
			companyIBAN: string
			companyNameBulgarian: string
			companyNameEnglish: string
			companyNumber: string
			companyVATNumber: string
			customerAddressLine0Bulgarian: string
			customerAddressLine0English: string
			customerAddressLine1Bulgarian: string
			customerAddressLine1English: string
			customerNameBulgarian: string
			customerNameEnglish: string
			customerNumber: string
			customerVATNumber: string
			invoiceDate?: string
			invoiceNumber: string
			softwareDevelopmentPriceExclVATInEuroBulgarian: string
			softwareDevelopmentPriceExclVATInEuroEnglish: string
			softwareDevelopmentPriceExclVATInLevBulgarian: string
			softwareDevelopmentTotalAmountInEuroEnglish: string
			softwareDevelopmentTotalAmountInLevBulgarian: string
			softwareDevelopmentVATAmountInEuroEnglish: string
		}
	} = {} as any
	try {
		config = JSON.parse((await fs.readFile(path.join(process.cwd(), 'invoiceConfig.json'))).toString())
	}
	catch(e) {
	}
	const monthToBulgarianMap = [
		'Януари',
		'Февруари',
		'Март',
		'Април',
		'Май',
		'Юни',
		'Юли',
		'Август',
		'Септември',
		'Октомври',
		'Ноември',
		'Декември'
	]
	let startDate = moment(
			(argv.d as string) ||
			(argv.date as string) ||
			moment().format('YYYY-MM-DD').toString(),
			'YYYY-MM-DD'
		).startOf('month'),
		endDate = startDate.clone().endOf('month').add(1, 'day'),
		currentDate = moment()

	// get the task stories
	console.log('Retrieving Asana comments...')
	let taskStories = (await request({
			method: 'get',
			headers: {'Authorization': `Bearer ${config.authToken}`},
			uri: `https://app.asana.com/api/1.0/tasks/${argv.t || argv.taskId || config.taskId}/stories?opt_fields=text,type`,
			json: true
		})).data,
		invoiceRows = [['date', 'project', 'task']],
		invoiceServices: {date: string, description: string, projectName: string}[][] = [[]],
		invoiceServiceGroupIndex = 0,
		startDateTimestamp = startDate.valueOf(),
		endDateTimestamp = endDate.valueOf()
	console.log('Asana comments retrieved successfully.')

	// find the comments in the task stories and extract the work data from them
	console.log('Processing Asana comments...')
	taskStories.forEach((story: {text: string, type: string}) => {
		if (story.type !== 'comment') {
			return
		}
		const textRows = story.text.match(/[^\r\n]+/g) || [],
			firstRow = textRows[0],
			textRowsLength = textRows.length
		let date = null
		if (textRowsLength > 1)
		// check if it's an evening recap
		if (!firstRow.match(/Evening recap/)) {
			return
		}
		// parse and confirm that the date is valid and within the interval
		date = moment(firstRow.split(' ')[0], 'DD.MM.YYYY')
		let dateTimestamp = date.valueOf()
		if (!date.isValid() || (dateTimestamp < startDateTimestamp) || (dateTimestamp > endDateTimestamp)) {
			return
		}
		// go through the comment rows for the day and prepare the invoice rows - by project and completed task
		let currentDate = date.format('Do MMMM YYYY').toString(),
			currentProjectName: string = ''
		for (let i = 1; i < textRowsLength; i++) {
			const textRowContext = textRows[i]
			// check if this row contains the project name; if it does, set it in the baseInvoiceRow and move on
			let projectNameMatch = textRowContext.match(/-(.+):/)
			if (projectNameMatch) {
				currentProjectName = projectNameMatch[1].replace(/^\s+|\s+$/g, '')
				continue
			}
			// check if this row contains the completed task name; if it does, add a new invoice row and move on
			let taskNameMatch = textRowContext.match(/->(.+)/)
			if (taskNameMatch) {
				const taskDescription = taskNameMatch[1].replace(/^\s+|\s+$/g, '')
				invoiceRows.push([currentDate, currentProjectName, taskDescription])
				if (
					(invoiceServiceGroupIndex === 0) && (invoiceServices[invoiceServiceGroupIndex].length === 24) ||
					(invoiceServiceGroupIndex === 1) && (invoiceServices[invoiceServiceGroupIndex].length === 25)
				) {
					invoiceServices.push([])
					invoiceServiceGroupIndex++
				}
				invoiceServices[invoiceServiceGroupIndex].push({
					date: currentDate,
					description: taskDescription,
					projectName: currentProjectName
				})
			}
		}
	})
	endDate.subtract(1, 'day')
	console.log('Asana comments processed successfully.')

	let hasOutputs = false
	// generate an XLSX file
	if (argv.xlsx) {
		console.log('Generating XLSX file...')
		let workbook = xlsx.utils.book_new()
		hasOutputs = true
		workbook.SheetNames.push('Invoice')
		workbook.Sheets.Invoice = xlsx.utils.aoa_to_sheet(invoiceRows)
		await new Promise<void> ((resolve) => {
			(xlsx as any).writeFileAsync(
				path.join(
					__dirname,
					`../output/${startDate.format('YYYY-MM-DD').toString()}_${endDate.format('YYYY-MM-DD').toString()}_invoice_services.xlsx`
				),
				workbook,
				{bookType:'xlsx', bookSST: false, type: 'binary'},
				() => resolve()
			)
		})
		console.log('XLSX file generated successfully.')
	}

	if (argv.docx || argv.pdf) {
		hasOutputs = true
		// populate the html template and generate the pdf data
		console.log('Generating invoice...')
		let templateLocals = Object.assign(
				{},
				config.templateData,
				{
					invoiceDate: config.templateData.invoiceDate || currentDate.format('DD.MM.YYYY').toString(),
					monthAndYearBulgarian: `${monthToBulgarianMap[startDate.month()]} ${startDate.format('YYYY').toString()}`,
					monthAndYearEnglish: startDate.format('MMMM YYYY').toString(),
					services: invoiceServices,
					styles: await new Promise((resolve, reject) => {
						sass.render({file: path.join(__dirname, './assets/styles.scss')}, (err, result) => {
							if (err) {
								reject(err)
							}
							resolve(result.css.toString())
						})
					})
				}
			),
			fullDataTemplateFilePath = path.join(__dirname, './assets/fullDataTemplate.pug'),
			mainDataTemplateFilePath = path.join(__dirname, './assets/mainDataTemplate.pug'),
			fullDataTemplate = (
				pug.compile(
					(await fs.readFile(fullDataTemplateFilePath)).toString(),
					{filename: fullDataTemplateFilePath}
				)
			)(templateLocals),
			mainDataTemplate = (
				pug.compile(
					(await fs.readFile(mainDataTemplateFilePath)).toString(),
					{filename: mainDataTemplateFilePath}
				)
			)(templateLocals),
			baseFileName = `${config.templateData.invoiceNumber} - ${startDate.format('MMMM YYYY').toString()}`
		
		console.log('Invoice generated successfully.')
		// export the populated html template as a DOCX file
		if (argv.docx) {
			console.log('Saving DOCX file...')
			await fs.writeFile(
				path.join(__dirname, `../output/${baseFileName}.docx`),
				docx.asBlob(fullDataTemplate)
			)
			console.log('DOCX file saved successfully.')
		}
		// export the populated html template as a PDF file
		if (argv.pdf) {
			const browser = await puppeteer.launch({ args: ['--no-sandbox'] })
			const page = await browser.newPage()
			console.log('Saving PDF file...')
			await page.setContent(fullDataTemplate)
			await page.pdf({
				path: path.join(__dirname, `../output/${baseFileName} (detailed).pdf`),
				height: '1200px',
				width: '1024px'
			})
			await page.setContent(mainDataTemplate)
			await page.pdf({
				path: path.join(__dirname, `../output/${baseFileName}.pdf`),
				height: '1200px',
				width: '1024px'
			})
			await browser.close()
			// await fs.writeFile(
			// 	path.join(__dirname, `../output/${baseFileName} (detailed).pdf`),
			// 	await new Promise((resolve, reject) => {
			// 		// pdf.create(
			// 		// 	fullDataTemplate,
			// 		// 	{
			// 		// 		height: '1200px',
			// 		// 		width: '1024px',
			// 		// 		// phantomPath: '/usr/local/bin/phantomjs'
			// 		// 		phantomPath: '/code/lemmsoft/asana-invoice-generator/code/node_modules/phantomjs-prebuilt/bin/phantomjs'
			// 		// 	}
			// 		// ).toBuffer((err, buffer) => {
			// 		// 	if (err) {
			// 		// 		reject(err)
			// 		// 	}
			// 		// 	resolve(buffer)
			// 		// })
			// 		const pdfStream = pdf(
			// 			fullDataTemplate,
			// 			{
			// 				pageHeight: '1200px',
			// 				pageWidth: '1024px'
			// 			}
			// 		)
			// 		const chunks: Buffer[] = []
			// 		pdfStream.on('data', (data) => chunks.push(Buffer.from(data)))
			// 		pdfStream.on('error', (err) => reject(err))
			// 		pdfStream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
			// 	})
			// )
			// await fs.writeFile(
			// 	path.join(__dirname, `../output/${baseFileName}.pdf`),
			// 	await new Promise((resolve, reject) => {
			// 		// pdf.create(
			// 		// 	mainDataTemplate,
			// 		// 	{
			// 		// 		height: '1200px',
			// 		// 		width: '1024px',
			// 		// 		// phantomPath: '/usr/local/bin/phantomjs'
			// 		// 		phantomPath: '/code/lemmsoft/asana-invoice-generator/code/node_modules/phantomjs-prebuilt/bin/phantomjs'
			// 		// 	}
			// 		// ).toBuffer((err, buffer) => {
			// 		// 	if (err) {
			// 		// 		reject(err)
			// 		// 	}
			// 		// 	resolve(buffer)
			// 		// })
			// 		const pdfStream = pdf(
			// 			mainDataTemplate,
			// 			{
			// 				pageHeight: '1200px',
			// 				pageWidth: '1024px'
			// 			}
			// 		)
			// 		const chunks: Buffer[] = []
			// 		pdfStream.on('data', (data) => chunks.push(Buffer.from(data)))
			// 		pdfStream.on('error', (err) => reject(err))
			// 		pdfStream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
			// 	})
			// )
			console.log('PDF file saved successfully.')
		}
	}

	if (!hasOutputs) {
		console.warn('No output options provided.')
	}
})().then(
	() => {
		console.log('Invoice gnerated successfully.')
		process.exit(0)
	},
	(err) => {
		console.log(err)
		process.exit(1)
	}
)
