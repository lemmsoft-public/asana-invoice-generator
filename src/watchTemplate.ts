import * as chokidar from 'chokidar'
import * as fs from 'fs-extra'
import * as path from 'path'
import * as pug from 'pug'
import * as sass from 'node-sass'

chokidar.watch(path.join(__dirname, '../src/assets/fullDataTemplate.pug'))
.on(
	'change',
	() => {
		(async function() {
			let styles = ''
			try {
				styles = (await fs.readFile(path.join(__dirname, '../test/styles.css'))).toString()
			}
			catch (e) {
			}
			const fullDataTemplateFilePath = path.join(__dirname, '../src/assets/fullDataTemplate.pug')
			await fs.writeFile(
				path.join(__dirname, '../test/fullDataTemplate.html'),
				pug.compile(
					(await fs.readFile(fullDataTemplateFilePath)).toString(),
					{filename: fullDataTemplateFilePath}
				)({
					companyAddressLine0Bulgarian: 'companyAddressLine0Bulgarian',
					companyAddressLine0English: 'companyAddressLine0English',
					companyAddressLine1Bulgarian: 'companyAddressLine1Bulgarian',
					companyAddressLine1English: 'companyAddressLine1English',
					companyIBAN: 'companyIBAN',
					companyNameBulgarian: 'companyNameBulgarian',
					companyNameEnglish: 'companyNameEnglish',
					companyNumber: 'companyNumber',
					companyVATNumber: 'companyVATNumber',
					customerAddressLine0Bulgarian: 'customerAddressLine0Bulgarian',
					customerAddressLine0English: 'customerAddressLine0English',
					customerAddressLine1Bulgarian: 'customerAddressLine1Bulgarian',
					customerAddressLine1English: 'customerAddressLine1English',
					customerNameBulgarian: 'customerNameBulgarian',
					customerNameEnglish: 'customerNameEnglish',
					customerNumber: 'customerNumber',
					customerVATNumber: 'customerVATNumber',
					invoiceDate: 'invoiceDate',
					invoiceNumber: 'invoiceNumber',
					monthAndYearBulgarian: 'monthAndYearBulgarian',
					monthAndYearEnglish: 'monthAndYearEnglish',
					services: [[
						{
							date: '01st March 2020',
							description: 'Test',
							projectName: 'Test Project'
						},
						{
							date: '01st March 2020',
							description: 'Test 2',
							projectName: 'Test 2 Project'
						},
						{
							date: '01st March 2020',
							description: 'Test 3',
							projectName: 'Test 3 Project'
						},
						{
							date: '01st March 2020',
							description: 'Test 4',
							projectName: 'Test 4 Project'
						}
					]],
					softwareDevelopmentPriceExclVATInEuroBulgarian: '50000',
					softwareDevelopmentPriceExclVATInEuroEnglish: '50000',
					softwareDevelopmentPriceExclVATInLevBulgarian: '50000',
					softwareDevelopmentTotalAmountInEuroEnglish: '50000',
					softwareDevelopmentTotalAmountInLevBulgarian: '50000',
					softwareDevelopmentVATAmountInEuroEnglish: '5000',
					softwareDevelopmentVATAmountInLevBulgarian: '50000',
					softwareDevelopmentVATPercent: '20',
					styles
				})
			)
		})().then(
			() => true,
			(err) => console.error(err)
		)
	}
)
.on('err', (data) => console.error(data))

chokidar.watch(path.join(__dirname, '../src/assets/styles.scss'))
.on(
	'change',
	() => {
		(async function() {
			await fs.writeFile(
				path.join(__dirname, '../test/styles.css'),
				await new Promise((resolve, reject) => {
					sass.render({file: path.join(__dirname, '../src/assets/styles.scss')}, (err, result) => {
						if (err) {
							reject(err)
						}
						resolve(result.css.toString())
					})
				})
			)
			await fs.utimes(path.join(__dirname, '../src/assets/fullDataTemplate.pug'), new Date(), new Date())
		})().then(
			() => true,
			(err) => console.error(err)
		)
	}
)
.on('err', (data) => console.error(data))
